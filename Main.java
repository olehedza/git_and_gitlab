public class Main {
    public static void main(String[] args) {
        prettyPrint("Oleksii");
    }

    static void prettyPrint(String name) {
        for (int i = 0; i < 30; i++) {
            System.out.print("-");
        }
        System.out.printf("\nHello, %s\n", name);
        for (int i = 0; i < 30; i++) {
            System.out.print("-");
        }
    }

    static void prettyPrint2(String name) {
        for (int i = 0; i < 30; i++) {
            System.out.print("*");
        }
        System.out.println();
        prettyPrint(name);
    }
}
